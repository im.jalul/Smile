'use strict';

function addCurveFromTopRightToTopLeft() { return 75; }
function addCurveFromTopLeftToTopRight() { return 8; }
function addCurveFromTopLeftToBottomRight() { return 5; }
function addLineFromTopToBottom() { return 99; }
function addLineFromBottomLineToBottomRight() { return 9; }
function addLineFromMiddleLineToMiddleRight() { return 3; }
function addLineFromTopLineToTopRight() { return -10; }
function addADotOnTop() { return 6; }
function addADotOnBottom() { return -66; }
function makeASymbolFrom(i) { return String.fromCharCode(i); }
function iCanDo(d) { return d.length; }
function doAll(fs) { return fs.map((f) => { return f() }).join(''); }
function write(ss) { console.log(ss.join('')); }

Array.prototype.remember = function(d) { this.push(d) }
Array.prototype.write = function() { console.log(this.join('')) }

const anEmptySpace = 0;
const iShould = {
  writeMajorS: function () {
    let c = anEmptySpace;
    c += addCurveFromTopRightToTopLeft();
    c += addCurveFromTopLeftToTopRight();

    return makeASymbolFrom(c);
  },

  writeMinorM: function () {
    let c = anEmptySpace;
    c += addLineFromTopToBottom();
    c += addCurveFromTopLeftToBottomRight();
    c += addCurveFromTopLeftToBottomRight();

    return makeASymbolFrom(c);
  },

  writeMinorI: function () {
    let c = anEmptySpace;
    c += addLineFromTopToBottom();
    c += addADotOnTop();

    return makeASymbolFrom(c);
  },

  writeMinorL: function () {
    let c = anEmptySpace;
    c += addLineFromTopToBottom();
    c += addLineFromBottomLineToBottomRight();

    return makeASymbolFrom(c);
  },

  writeMinorE: function () {
    let c = anEmptySpace;
    c += addLineFromTopToBottom();
    c += addLineFromBottomLineToBottomRight();
    c += addLineFromMiddleLineToMiddleRight();
    c += addLineFromTopLineToTopRight();

    return makeASymbolFrom(c);
  },

  writeExclamationMark: function () {
    let c = anEmptySpace;
    c += addLineFromTopToBottom();
    c += addADotOnBottom();

    return makeASymbolFrom(c);
  },
};

function howDoIWriteASmile() {
  const whatShouldIDo = [
    iShould.writeMajorS,
    iShould.writeMinorM,
    iShould.writeMinorI,
    iShould.writeMinorL,
    iShould.writeMinorE,
    iShould.writeExclamationMark,
  ];

  if (iCanDo(whatShouldIDo)) {
    const whatIAchieved = doAll(whatShouldIDo);
    return whatIAchieved;
  } else {
    return '';
  }
}

let howManyTimes = 3;
let memory = [];

for (; howManyTimes--; memory.remember(howDoIWriteASmile())) {}
for (; memory.length; memory.write(), memory.shift()) {}

/*
 *  Command
 *  > node ./smile
 *
 *  Output Should Be
 *
 *  Smile!Smile!Smile!
 *  Smile!Smile!
 *  Smile!
 *
 */
