# Smile!

I have a solution for this question.
> How do I write a program that produces the following output?
>

> ```
Smile!Smile!Smile!
Smile!Smile!
Smile!
```
(Original link:  https://www.quora.com/Homework-Question-How-do-I-write-a-program-that-produces-the-following-output-1)

Using JavaScript, I just make it for fun. Smile :)
```
function howDoIWriteASmile() {
  const whatShouldIDo = [
    iShould.writeMajorS,
    iShould.writeMinorM,
    iShould.writeMinorI,
    iShould.writeMinorL,
    iShould.writeMinorE,
    iShould.writeExclamationMark,
  ];
  if (iCanDo(whatShouldIDo)) {
    const whatIAchieved = doAll(whatShouldIDo);
    return whatIAchieved;
  } else {
    return '';
  }
}
```

Try it.
```
 node ./smile
```
